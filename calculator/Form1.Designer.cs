﻿namespace calc
{
    partial class CalculatorForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstOperand = new System.Windows.Forms.TextBox();
            this.secondOperand = new System.Windows.Forms.TextBox();
            this.addition = new System.Windows.Forms.Button();
            this.lblOperator = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblErgebnis = new System.Windows.Forms.Label();
            this.subtraction = new System.Windows.Forms.Button();
            this.mean = new System.Windows.Forms.Button();
            this.power = new System.Windows.Forms.Button();
            this.max = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstOperand
            // 
            this.firstOperand.Location = new System.Drawing.Point(24, 35);
            this.firstOperand.Name = "firstOperand";
            this.firstOperand.Size = new System.Drawing.Size(101, 23);
            this.firstOperand.TabIndex = 0;
            this.firstOperand.Text = "0";
            this.firstOperand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // secondOperand
            // 
            this.secondOperand.Location = new System.Drawing.Point(202, 35);
            this.secondOperand.Name = "secondOperand";
            this.secondOperand.Size = new System.Drawing.Size(101, 23);
            this.secondOperand.TabIndex = 0;
            this.secondOperand.Text = "0";
            this.secondOperand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // addition
            // 
            this.addition.Location = new System.Drawing.Point(24, 138);
            this.addition.Name = "addition";
            this.addition.Size = new System.Drawing.Size(138, 42);
            this.addition.TabIndex = 1;
            this.addition.Text = "Addition";
            this.addition.UseVisualStyleBackColor = true;
            this.addition.Click += new System.EventHandler(this.btnAddition_Click);
            // 
            // lblOperator
            // 
            this.lblOperator.AutoSize = true;
            this.lblOperator.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblOperator.Location = new System.Drawing.Point(148, 43);
            this.lblOperator.Name = "lblOperator";
            this.lblOperator.Size = new System.Drawing.Size(0, 15);
            this.lblOperator.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label2.Location = new System.Drawing.Point(24, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ergebnis:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblErgebnis
            // 
            this.lblErgebnis.BackColor = System.Drawing.Color.Transparent;
            this.lblErgebnis.Location = new System.Drawing.Point(104, 83);
            this.lblErgebnis.Name = "lblErgebnis";
            this.lblErgebnis.Size = new System.Drawing.Size(177, 22);
            this.lblErgebnis.TabIndex = 2;
            this.lblErgebnis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // subtraction
            // 
            this.subtraction.Location = new System.Drawing.Point(168, 138);
            this.subtraction.Name = "subtraction";
            this.subtraction.Size = new System.Drawing.Size(135, 42);
            this.subtraction.TabIndex = 1;
            this.subtraction.Text = "Subtraktion";
            this.subtraction.UseVisualStyleBackColor = true;
            this.subtraction.Click += new System.EventHandler(this.btnSubtraktion_Click);
            // 
            // mean
            // 
            this.mean.Location = new System.Drawing.Point(24, 186);
            this.mean.Name = "mean";
            this.mean.Size = new System.Drawing.Size(138, 42);
            this.mean.TabIndex = 1;
            this.mean.Text = "Mittelwert";
            this.mean.UseVisualStyleBackColor = true;
            this.mean.Click += new System.EventHandler(this.btnMittelwert_Click);
            // 
            // power
            // 
            this.power.Location = new System.Drawing.Point(168, 186);
            this.power.Name = "power";
            this.power.Size = new System.Drawing.Size(135, 42);
            this.power.TabIndex = 1;
            this.power.Text = "Potenz";
            this.power.UseVisualStyleBackColor = true;
            this.power.Click += new System.EventHandler(this.btnPotenz_Click);
            // 
            // max
            // 
            this.max.Location = new System.Drawing.Point(104, 234);
            this.max.Name = "max";
            this.max.Size = new System.Drawing.Size(138, 42);
            this.max.TabIndex = 1;
            this.max.Text = "Maximum";
            this.max.UseVisualStyleBackColor = true;
            this.max.Click += new System.EventHandler(this.btnMaximum_Click);
            // 
            // CalculatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 301);
            this.Controls.Add(this.lblErgebnis);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblOperator);
            this.Controls.Add(this.max);
            this.Controls.Add(this.power);
            this.Controls.Add(this.mean);
            this.Controls.Add(this.subtraction);
            this.Controls.Add(this.addition);
            this.Controls.Add(this.secondOperand);
            this.Controls.Add(this.firstOperand);
            this.Name = "CalculatorForm";
            this.Text = "Taschenrechner";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        // Entry fields
        private System.Windows.Forms.TextBox firstOperand;
        private System.Windows.Forms.TextBox secondOperand;
        
        // Label definitions
        private System.Windows.Forms.Label lblOperator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblErgebnis;
        
        // Button definitions
        private System.Windows.Forms.Button max;
        private System.Windows.Forms.Button power;
        private System.Windows.Forms.Button mean;
        private System.Windows.Forms.Button subtraction;
        private System.Windows.Forms.Button addition;
    }
}